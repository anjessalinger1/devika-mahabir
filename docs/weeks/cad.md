**3D DESIGN**

**TINKERCAD**

To create a 3D digital design you go to Tinkercad

**1. First you click on 3D design and then "create new design"**

![tag](../img/45.png)

**2. You can use all kinds of shapes to design**

![tag](../img/33.png)


**3. On the left you can either chose to copy, paste and delete your shape/design**
   **You can also look at your design from every perspective**

![tag](../img/70.png)

**4. To create a hole into your design you have to chose a shape and click on hole**

![tag](../img/46.png)

**5. Then you select all with ctrl+A and click on "Group"**

![tag](../img/47.png)

![tag](../img/48.png)

**6. When you want to change the size of your design you click on it and change it**

![tag](../img/50.png)

**7. When you're finished you export your design as an stl file**

![tag](../img/51.png)



**FUSION 360**
  
Fusion 360 is a cloud-based CAD/CAM tool for collaborative product development that combines industrial design, 
mechanical engineering, and machine tool programming into one software solution.

**1. When you open Fusion 360 it should look like this.**

![tag](../img/52.png)

**2. To create a sketch you have to click on sketch. Then you select "create => Rectangle => Center Rectangle"**
**To create a circle select "Create => Circle"**

![tag](../img/54.png)

**3. Then you select on "Modify => Offset " to create a offset and then finish your sketch.**

![tag](../img/55.png)

**4. Now you click on "Extrude" and select what you want to extrude.**

![tag](../img/57.png)

![tag](../img/58.png)

**5. To create another extrude select this and your profile that you want to extrude**

![tag](../img/59.png)

![tag](../img/60.png)

**6. To save**

![tag](../img/61.png)

**7. To export as an stl file**

![tag](../img/62.png)

     

