### Electronic Desgin

This week we learned to design **atmega328p microcontroller** board using **KiCad And Flatcam** 


**TO DO**

- Create New Project & Sketch KICAD
- Import Fab Library
- Import Components
- Connect Components
- Give Components correct name and value
- Annotate the design
- Assign Footprint to Symbols
- Generate Netlist
- Open Pcb New
- Read in Generate netlist
- In Pcb New Set Design Rules (Setup -Design Rules)
- Rearrange Components and Make Traces
- Place your Auxiliary Axis
- Plot Layers


When you open **KiCad**

  - New-->Project
  - Preferences-->Manage Symbol Libraries


Then you Import the **Fab.lib**

![tag](../img/51 - Copy.png)

![tag](../img/53.png)

**Now you select all you components**

**Compenent List**

- Atmega328p-Au
- 2x Resistor
- 1x 1uF Capacitor
- 1x 0.1uF Capacitor
- Led
- Switch
- Resinator
- 3x Header

![tag](../img/23.png)

- **Annotate** to check if I had my tags labeled correctly

![tag](../img/56.png)

 I selected **assign footprint** to pcb and added my footprint.

  - FootPrintLib-->Fab.pretty
  - Fab.pretty-->Fab.mod


![tag](../img/30.png)

- Make schematic in **KiCad**

![tag](../img/at.png)

![tag](../img/att.png)

![tag](../img/pcb.png)

- Select **Generate netlist** and import netlist to Pcbnew

![tag](../img/net.png)

![tag](../img/nett.png)

- KiCad Organize your components. Use **Route tracs** , **Auxiliary Axis** and **dimension**

![tag](../img/nettt.png)

 KiCad **Plot**

  - FrontCut
  - Output Directory
  - Aux As Orgin


![tag](../img/abc.png)

 - After plotting and generating my gerber files I then opened **Flatcam** to add parameters
 
 ![tag](../img/cap.png)
 
- File --> Open Gerber

 ![tag](../img/42.png)
 
- Go to you Gerber **options** and select **mm**

![tag](../img/43.png)

- Select Gerber

**Isolation Routing**

- Tool dia - 0.2
- width (passes) 1
- Pass overlay 0.170000

**Board Cutout**

- Tool dia - 0.8
- Margin: 1.5
- Gap size: 0.5

![tag](../img/abcd.png)

For the **Front Cut** select

- Cut Z  -0.12
- Travel Z 2.0
- Feedrate  30mm/min
- Tool dia 0.2
- Spindle-speed 1000

![tag](../img/abdc.png)

**Then Export Gcode**

For the **Edge Cut** select

- Cut Z  -1.5  
- Travel Z 0.1
- Feedrate  20 mm/min
- Tool dia 0.8
- Spindle-speed 1000
- Multi Depth (check) 
- Depth pass 0.5

![tag](../img/bacd.png)

**Export Gcode**

![tag](../img/dcba.png)
